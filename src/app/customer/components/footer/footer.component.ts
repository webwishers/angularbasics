import { AuthService } from './../../../shared/services/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  isLogin: any;
  constructor(private auth: AuthService) { }

  ngOnInit() {
    this.isLogined();
  }

  isLogined() {
    this.isLogin = this.auth.isCutomerLogined();
    console.log(this.isLogin);
  }

}
