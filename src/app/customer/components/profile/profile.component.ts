import { UserService } from './../../../shared/services/user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  userInfo: any;

  constructor(private user: UserService) { }

  ngOnInit() {
    this.userInfo = this.user.getActiveUser();
  }

}
