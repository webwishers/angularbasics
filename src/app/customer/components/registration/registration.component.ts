import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators  } from '@angular/forms';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  regForm: FormGroup;
  submitted = false;
  getStateList: any;
  doesMatch = true;
  registeredUsers: any;

  countries = [
    {
      id: 1,
      name: 'india'
    },
    {
      id: 2,
      name: 'usa'
    }
  ];

  states = [
    {
      id: 1,
      name: 'kerala',
      parent: 1
    },
    {
      id: 2,
      name: 'goa',
      parent: 1
    },
    {
      id: 3,
      name: 'washington',
      parent: 2
    },
    {
      id: 4,
      name: 'alaska',
      parent: 2
    }
  ];

  constructor(fb: FormBuilder) {
    this.regForm = fb.group({
      firstname: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
      lastname: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      // tslint:disable-next-line:max-line-length
      mobile: ['', Validators.compose([Validators.required, Validators.pattern('^([0-9]{10})$')])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(25)])],
      confirmPassword: ['', Validators.required],
      // tslint:disable-next-line:max-line-length
      dob: ['', Validators.compose([Validators.required])],
      gender: ['', Validators.required],
      country: ['', Validators.required],
      state: ['', Validators.required],
      terms: ['', Validators.compose([Validators.required])]
    });
  }

  ngOnInit() {
  }

  get firstname() { return this.regForm.get('firstname'); }
  get lastname() { return this.regForm.get('lastname'); }
  get email() { return this.regForm.get('email'); }
  get mobile() { return this.regForm.get('mobile'); }
  get password() { return this.regForm.get('password'); }
  get dob() { return this.regForm.get('dob'); }
  get gender() { return this.regForm.get('gender'); }
  get country() { return this.regForm.get('country'); }
  get state() { return this.regForm.get('state'); }
  get terms() { return this.regForm.get('terms'); }
  get confirmPassword() { return this.regForm.get('confirmPassword'); }

  saveRegForm(event) {
    this.submitted = true;
    if (this.regForm.invalid) {
      return;
    }

    const pwd = this.regForm.get('password').value;
    const cpwd = this.regForm.get('confirmPassword').value;
    if (pwd !== cpwd) {
      this.doesMatch = false;
      return;
    }
    this.registeredUsers = localStorage.getItem('registered_users') ? JSON.parse(localStorage.getItem('registered_users')) : [];
    this.registeredUsers.push(this.regForm.value);
    localStorage.setItem('registered_users', JSON.stringify(this.registeredUsers));
  }

  getStates(event) {
    this.getStateList = [];
    this.states.map((val, key) => {
      if (parseInt(event.target.value, 10) === this.states[key]['parent']) {
        this.getStateList.push(val);
      }
    });
  }

}
