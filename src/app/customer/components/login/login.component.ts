import { AuthService } from './../../../shared/services/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  isLogin = false;
  loginStatus: any;
  errorMsg: string;
  submitted = false;

  constructor(fb: FormBuilder, private auth: AuthService, private router: Router) {
    this.loginForm = fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required])]
    });
  }

  ngOnInit() {
  }

  get email() { return this.loginForm.get('email'); }
  get password() { return this.loginForm.get('password'); }

  authLogin(event) {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    const email = this.loginForm.get('email').value;
    const password = this.loginForm.get('password').value;
    this.loginStatus = this.auth.userLogin(email, password);

    if (this.loginStatus && this.loginStatus.length > 0) {
      this.isLogin = true;
      this.router.navigate(['/profile']);
    } else {
      this.errorMsg = 'Email/Password mismatch';
    }
  }
}
