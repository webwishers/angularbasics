import { AuthService } from './../../../shared/services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isLogin: any;
  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit() {
    this.router.events.pipe(filter((event: any) => event instanceof NavigationEnd)).subscribe(x => {
      this.isLogined();
    });
  }

  isLogined() {
    this.isLogin = this.auth.isCutomerLogined();
    console.log('HEADER', this.isLogin);
  }

  logout() {
    localStorage.removeItem('customer_login');
    this.router.navigate(['/']);
  }

}
