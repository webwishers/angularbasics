import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { UsersComponent } from './components/users/users.component';
import { AdminLoginComponent } from './components/admin-login/admin-login.component';
import { AdminNavComponent } from './components/admin-nav/admin-nav.component';
import { SearchUserPipe } from '../shared/pipes/search-user.pipe';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

const routes: Routes = [
  {
    path: 'admin', component: AdminLoginComponent
  },
  {
    path: 'dashboard', component: DashboardComponent
  },
  {
    path: 'users', component: UsersComponent
  }
];

@NgModule({
  declarations: [
    AdminLoginComponent,
    DashboardComponent,
    UsersComponent,
    AdminNavComponent,
    AdminLoginComponent,
    AdminNavComponent,
    SearchUserPipe
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    NgSelectModule
  ]
})
export class AdminModule { }
