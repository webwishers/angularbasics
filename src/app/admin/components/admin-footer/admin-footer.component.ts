import { Component, OnInit } from '@angular/core';
import { AuthService } from './../../../shared/services/auth.service';

@Component({
  selector: 'app-admin-footer',
  templateUrl: './admin-footer.component.html',
  styleUrls: ['./admin-footer.component.scss']
})
export class AdminFooterComponent implements OnInit {

  isLogin: any;
  constructor(private auth: AuthService) { }

  ngOnInit() {
    this.isLogined();
  }

  isLogined() {
    this.isLogin = this.auth.isAdminLogined();
    console.log(this.isLogin);
  }

}
