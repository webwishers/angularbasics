import { Component, OnInit } from '@angular/core';
import { AuthService } from './../../../shared/services/auth.service';

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.scss']
})
export class AdminHeaderComponent implements OnInit {

  isLogin: any;
  constructor(private auth: AuthService) { }

  ngOnInit() {
    this.isLogined();
  }

  isLogined() {
    this.isLogin = this.auth.isAdminLogined();
    console.log(this.isLogin);
  }

}
