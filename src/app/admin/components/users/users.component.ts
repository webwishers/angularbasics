import { UserService } from './../../../shared/services/user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  searchText: string;
  userList: any;
  constructor(private user: UserService) { }

  ngOnInit() {
    if (this.user.getUsers() && this.user.getUsers().length > 0) {
      this.userList = this.user.getUsers();
    }
  }

}
