import { UserService } from './../../../shared/services/user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  selectedStates = [];
  countries = [
    {
      id: 1,
      name: 'india'
    },
    {
      id: 2,
      name: 'usa'
    }
  ];

  states = [
    {
      id: 1,
      name: 'kerala',
      parent: 1
    },
    {
      id: 2,
      name: 'goa',
      parent: 1
    },
    {
      id: 3,
      name: 'washington',
      parent: 2
    },
    {
      id: 4,
      name: 'alaska',
      parent: 2
    }
  ];

  blogs: any;

  constructor(private user: UserService) { }

  ngOnInit() {
    this.user.getTeams({'post_status': 'publish'}).subscribe( response => {
      this.blogs = response['blog_result'];
    });
  }

  getStates(countryId) {
    this.selectedStates = [];
    this.states.map((val, key) => {
      if (this.states[key]['parent'] === countryId) {
        this.selectedStates.push(val);
      }
    });
  }

}
