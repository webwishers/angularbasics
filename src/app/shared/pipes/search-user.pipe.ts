import { filter } from 'rxjs/operators';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchUser'
})
export class SearchUserPipe implements PipeTransform {

  transform(value: any, searchText: any): any {

    if (value) {
      if ( (value.filter( item => item.firstname.search(new RegExp(searchText, 'i')) > -1).length > 0 )) {
        return value.filter( item => item.firstname.search(new RegExp(searchText, 'i')) > -1);
      } else if ( (value.filter( item => item.lastname.search(new RegExp(searchText, 'i')) > -1).length > 0 )) {
        return value.filter( item => item.lastname.search(new RegExp(searchText, 'i')) > -1);
      } else if ( (value.filter( item => item.email.search(new RegExp(searchText, 'i')) > -1).length > 0 )) {
        return value.filter( item => item.email.search(new RegExp(searchText, 'i')) > -1);
      } else {
        return [];
      }
    } else {
      return [];
    }
  }

}
