import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userInfo = [];
  regUsers: any;
  isAdminLogin: any;
  constructor() { }

  isCutomerLogined() {
    return localStorage.getItem('customer_login') ? true : false;
  }

  userLogin(email, password)  {
    this.userInfo = [];
    localStorage.removeItem('customer_login');
    this.regUsers = JSON.parse(localStorage.getItem('registered_users'));
    if (this.regUsers) {
      this.regUsers.map( (value, key) => {
        if (this.regUsers[key]['email'] === email && this.regUsers[key]['password'] === password) {
          this.userInfo.push(this.regUsers[key]);
          localStorage.setItem('customer_login', JSON.stringify(this.userInfo));
        }
      });
    }
    return this.userInfo;
  }

  isAdminLogined() {
    return localStorage.getItem('admin_login') ? true : false;
  }

  adminLogin() {
    if (this.isAdminLogin) {
      if (this.isAdminLogin['isLogin'] === true && this.isAdminLogin['email'] === 'lek@gmail.com') {
        return true;
      }
      return false;
    }
  }
}
