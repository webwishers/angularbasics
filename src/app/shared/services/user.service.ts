import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getUsers() {
    return JSON.parse(localStorage.getItem('registered_users'));
  }

  getActiveUser() {
    return JSON.parse(localStorage.getItem('customer_login'));
  }

  getTeams(data) {
    return this.http.post('https://evolvegt.com/evolve-api/public/getall/blogs', data);
  }
}
